param(
    [Parameter(HelpMessage="Parameter help description")]
    [int32]
    $Depth=-1
)

$AllAttributes = "Archive, Compressed, Device, Directory, Encrypted," + `
    "Hidden, Normal, NotContentIndexed, Offline, ReadOnly, ReparsePoint, SparseFile, System, Temporary"

if ($Depth -gt 0){
    $childItems = Get-ChildItem -Attributes $AllAttributes -Directory -Recurse -Depth $Depth -Include ".git"
}
else {
    $childItems = Get-ChildItem -Attributes $AllAttributes -Directory -Recurse -Include ".git"
}

$dirty=@()
foreach($gitDirectory in $childItems){
    $repoDirectory = $gitDirectory.Parent
    Push-Location $repoDirectory.FullName
    $status = git status -s
    if ($status.Length -gt 0 -or $LASTEXITCODE -ne 0)
    {
        #echo ***********************************
        #pwd
        #echo $LASTEXITCODE
        #echo $status
        #echo ***********************************
        Write-Host $repoDirectory.FullName
        $status | Out-String -Stream | Write-Verbose
        $dirty += $repoDirectory
    }
    #
    #echo ***************
    #echo $status
    #echo ***************
    Pop-Location
}
#foreach($item in $dirty)
#{
#    echo $item.FullName
#}
