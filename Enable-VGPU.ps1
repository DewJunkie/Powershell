Function Select-VMGpuPartitionAdapterFriendlyName {
    "Getting gpu list, this takes a minute..."
    $list = Get-VMHostPartitionableGpu | % {
        $parts = $_.Name.Split('#')
        $gpu = Get-WmiObject Win32_PNPSignedDriver | Where-Object { ($_.HardwareID -eq "PCI\$($parts[1])") }
        [pscustomobject]@{
            Name = $gpu.Description
            Gpu  = $_
        }
    }
    # We don't want to show the selection until we have all of them, seems to hand if you select one before the full list is populated
    $list | Out-GridView -OutputMode Single
}

function Add-VMGPU {
    param(
        [string]$VMName,
        [string]$GPUName,
        [decimal]$GPUResourceAllocationPercentage = 100
    )
    Write-Output "Getting GPUs"
    $PartitionableGPUList = Get-WmiObject -Class "Msvm_PartitionableGpu" -ComputerName $env:COMPUTERNAME -Namespace "ROOT\virtualization\v2" 
    $PartitionableGPUList | ft
    if ($GPUName -eq "AUTO") {
        Write-Output "Auto"
        $DevicePathName = $PartitionableGPUList.Name[0]
        Write-Output "$vmname => $DevicePathName"
        Add-VMGpuPartitionAdapter -VMName $VMName -InstancePath $DevicePathName
    }
    else {
        Write-Output $GPUName
        $DeviceID = ((Get-WmiObject Win32_PNPSignedDriver | where { ($_.Devicename -eq "$GPUNAME") }).hardwareid).split('\')[1]
        Write-Output "$vmname => $DeviceID"
        $DevicePathName = ($PartitionableGPUList | Where-Object name -like "*$deviceid*").Name
        Write-Output "$vmname => $DevicePathName"
        Add-VMGpuPartitionAdapter -VMName $VMName -InstancePath $DevicePathName
    }
        
    #$multiplier = [math]::round($GPUResourceAllocationPercentage / 100, 2)
    #
    #Set-VMGpuPartitionAdapter -VMName $VMName -MinPartitionVRAM ([math]::round($(1000000000 / $devider))) -MaxPartitionVRAM ([math]::round($(1000000000 / $devider))) -OptimalPartitionVRAM ([math]::round($(1000000000 / $devider)))
    #Set-VMGPUPartitionAdapter -VMName $VMName -MinPartitionEncode ([math]::round($(18446744073709551615 / $devider))) -MaxPartitionEncode ([math]::round($(18446744073709551615 / $devider))) -OptimalPartitionEncode ([math]::round($(18446744073709551615 / $devider)))
    #Set-VMGpuPartitionAdapter -VMName $VMName -MinPartitionDecode ([math]::round($(1000000000 / $devider))) -MaxPartitionDecode ([math]::round($(1000000000 / $devider))) -OptimalPartitionDecode ([math]::round($(1000000000 / $devider)))
    #Set-VMGpuPartitionAdapter -VMName $VMName -MinPartitionCompute ([math]::round($(1000000000 / $devider))) -MaxPartitionCompute ([math]::round($(1000000000 / $devider))) -OptimalPartitionCompute ([math]::round($(1000000000 / $devider)))
    
}

$vm = Get-VM | Out-GridView -OutputMode Single
$gpu = Select-VMGpuPartitionAdapterFriendlyName
Write-Output "$($vm.Name) => $($gpu.Name)"
Add-VMGPU -VMName $vm.Name -GPUName $gpu.Name
