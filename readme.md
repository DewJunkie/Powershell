# Useful powershell scripts

|Script             |Description|
|-                  |-          |
|Find-Git-Dirty.ps1 |Recursively search for git repositories with uncommited changes|
