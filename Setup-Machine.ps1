# if $profile does not exist
# New-Item -ItemType File -Force $Profile
#Profile contents
<#
Import-Module PSReadLine

Set-PSReadlineKeyHandler -Key Tab -Function Complete
# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}

Set-Alias notepad 'C:\Program Files\Notepad++\notepad++.exe'
Set-Alias vi 'C:\Program Files (x86)\vim\vim80\vim.exe'
#>

Install-Module PSReadline

iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

# set a common cache location
choco config set cacheLocation \\dabmi2\Software\chococache\

# Essentials
choco install -y vim 7zip notepadplusplus spacesniffer ChocolateyGUI adobereader

# Developer stuff
choco install -y GoogleChrome git steam terminals tortoisegit virtualbox nmap meld 
choco install -y Cygwin msys2 sysinternals libreoffice Firefox
choco install -y lastpass linqpad5
choco install -y jetbrainstoolbox wireshark

# Server Stuff
choco install -y sysinternals gsmartcontrol 

# Packages
#choco install -y MsSqlServerManagementStudio2014Express
choco install -y syncthing

# upgrade all installed packages
choco upgrade -y all
