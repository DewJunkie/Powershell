param(
    [Parameter(HelpMessage="Parameter help description")]
    [string]
    $source,
    [string]
    $target
)

if ($source.EndsWith('\'))
{
    $source = $source.Substring(0, $source.Length - 1);
    Write-Output $source
}

$sw=[System.Diagnostics.Stopwatch]::StartNew()

$myName=$MyInvocation.InvocationName

"{0} $source => $target" -f $sw.Elapsed.ToString() | Write-Verbose
$sourceFolder = Get-Item($source) -Force
$targetFolder = $sourceFolder.FullName.Replace($sourceFolder.Root.Name, $target)

if ([System.IO.Directory]::Exists($targetFolder))
{
    Write-Error "Target Directory exists."
    #exit -1
}

"{0} $sourceFolder => $targetFolder" -f $sw.Elapsed.ToString() | Write-Verbose
#Copy-Item -Recurse $sourceFolder $targetFolder 
write-Output "$sourceFolder $targetFolder"
#return
Robocopy.exe /move /copyall /zb /e /sl /r:2 /w:1 /nfl $sourceFolder $targetFolder

"{0} Copy Done" -f $sw.Elapsed.ToString() | Write-Verbose

#$sourceItems = Get-ChildItem -Recurse $source
#$targetItems = Get-ChildItem -Recurse $targetFolder

#"{0} Starting compare" -f $sw.Elapsed | Write-Verbose

#$diffs = Compare-Object $sourceItems $targetItems

#if ($diffs)
#{
#    $diffs
#}
#else {
    #Robocopy.exe /move /copyall /zb /e /xj /r:2 /w:1 /nfl $sourceFolder $targetFolder
    New-Item -ItemType SymbolicLink $sourceFolder -Target $targetFolder
    "{0} Success" -f $sw.Elapsed | Write-Output
#}

$appdata=[System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::CommonApplicationData)
Write-Output $appdata
Add-Content "$appdata\.$MyName.log" "$source => $target"